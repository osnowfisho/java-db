/*
print out the table
*/

import java.util.*;
import java.io.*;

class Printing {
	public static void main(String[] args) {
		Printing print = new Printing();
		print.run();
	}

	public void printTable(Table table) {
		String s;
		
		for (int i=0; i<table.getFieldNameSize(); i++) {
			s = table.getFieldName().getField(i);
			System.out.printf("%-10s", s);
		}
		System.out.println();
		for (int i=0; i<table.getRowNumber(); i++) {
			for (int j=0; j<table.getFieldNameSize(); j++) {
				s = table.selectRow(i).getField(j);
				System.out.printf("%-10s", s);
			}
			System.out.println();
		}
	}

	private void run () {
		testPrint();
	}

	private void testPrint() {
		StoreFile file = new StoreFile();
		Table table = new Table();
		
		assert(file.readFile(table, "pet.txt"));
		printTable(table);
	}
}