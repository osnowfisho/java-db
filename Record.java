/*
add, set, update a field and find out how many strings it has
*/

import java.util.*;

class Record {
	private ArrayList<String> field = new ArrayList<String>();

	public static void main (String[] args) {
		Record record = new Record();
		record.run();
	}

	public boolean setField(String... str) {
		try {
			for (String s: str) {
				field.add(s);
			}
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public boolean updateField(int i, String s) {
		try {
			if (field.set(i, s) != null) {
				return true;
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	// public void deleteField(int i) {
	// 	field.remove(i);
	// }

	public String getField(int i) {
		return field.get(i);
	}

	public int getFieldSize() {
		return field.size();
	}

	public void clearAll() {
		field.clear();
	}

	// public ArrayList<String> getArrList() {
	// 	return field;
	// }

	private void run() {
		testSet();
		testGet();
		testUpdate();
		// testDelete();
	}

	private void testSet() {
		assert(setField("1", "Fido", "dog", "ab123"));
	}

	private void testGet() {
		assert(getField(0).equals("1"));
		assert(getField(1).equals("Fido"));
		assert(getField(2).equals("dog"));
		assert(getField(3).equals("ab123"));
	}

	private void testUpdate() {
		assert(updateField(0, "2"));
		assert(updateField(1, "Wanda"));
		assert(updateField(2, "fish"));
		assert(updateField(3, "ef789"));
		assert(!updateField(4, "ab123"));
		assert(!getField(0).equals("1"));
		assert(getField(1).equals("Wanda"));
		assert(!getField(2).equals("dog"));
		assert(getField(3).equals("ef789"));
	}

	// private void testDelete() {
	// 	deleteField(2);
	// 	assert(!getField(2).equals("fish"));
	// 	assert(getField(2).equals("ef789"));
	// }
}
