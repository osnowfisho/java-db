/*
create a new folder and can read multiple files in one go
*/

import java.util.*;
import java.io.*;

class Database {
	File f = new File("New_Folder");

	public static void main(String[] args) {
		Database db = new Database();
		db.run();
	}

	private void createFolder() {
		try {
			if (f.mkdir()) { 
				System.out.println("Directory Created");
			}
			else {
				System.out.println("Directory is not created");
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void readMulFiles() {
		StoreFile file = new StoreFile();
		Printing print = new Printing();
		File[] files = f.listFiles();
		int n = files.length;
		Table t[] = new Table[n];

		if (n > 0) {
			for (int i=0; i<n; i++) {
				t[i] = new Table();
				file.readFile(t[i], "New_Folder/" + files[i].getName());
				print.printTable(t[i]);
			}
		}
		else {
			System.out.println("No files in directory");
		}
	}

	private void run() {
		StoreFile sf = new StoreFile();
		Table table = new Table();

		createFolder();

		assert(table.setFieldName("Id", "Name", "Kind", "Owner"));
		assert(table.insertRow("1", "Fido", "dog", "ab123"));
		assert(table.insertRow("2", "Wanda", "fish", "ef789"));
		assert(table.insertRow("3", "Garfield", "cat", "ab123"));
		assert(sf.writeFile(table, "New_Folder/pet.txt"));

		table.getFieldName().clearAll();
		while (table.getRowNumber() > 0) {
			assert(table.deleteRow(0));
		}

		assert(table.setFieldName("Username", "Name"));
		assert(table.insertRow("ab123", "Jo"));
		assert(table.insertRow("cd456", "Sam"));
		assert(table.insertRow("ef789", "Amy"));
		assert(table.insertRow("gh012", "Pete"));
		assert(sf.writeFile(table, "New_Folder/name.txt"));

		readMulFiles();
	}
}