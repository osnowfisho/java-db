### Records
`Record.java`

For the `Record` part, I did as the instructions said: I created an arraylist to store, add, get data and find out how many strings is has.

I set 4 methods to add, get, update,and return the string size using the arraylist methods.

* `public void setField(String... str)` is the setter
* `public void updateField(int i, String s)` is to update the data in the block
* `public String getField(int i)` is the getter
* `public int getFieldSize()` is to return the number of columns

I also wrote some methods such as `public void deleteField(int i)` to delete the data in one block and `public ArrayList<String> getArrList()` to return the whole arraylist to print out the whole arraylist in the `Table` class when I was testing at first, but then realised that they are not necessary (and aren't asked in the assignment) so I commented those methods out.


### Tables
`Table.java`

At first, I made an arraylist of `String` just like what I did in `Record` class and tried putting in and printing out data but then realised that's not how it's supposed to be done. So I changed the data type to arraylist of `Record` and made changes to the methods I wrote. Most of the purpose of the methods in `Table` class are the same as the methods in the `Record` class but they make changes to the whole row rather than each block of data.

* `public void setFieldName(String... s)`
* `public void insertRow(String... s)`
* `public Record selectRow(int i)` is to select and return a row
* `public void deleteRow(int i)`
* `public void updateRow(int i, String... s)`
* `public int getFieldNameSize()` is also to return the number of columns
* `public void alterTable(String s)` is to add a new column to the table
* `public Record getFieldName()` is to return the whole row of field name

For now most of the methods are all void type and I might need to change that in the future or else if something wrong happens the program may crash, but at the moment I'm happy with these methods. Also I don't have a method to make sure that the input data has the same number as the column number yet.


### Files
`StoreFile.java`

I read files in using `BufferedReader`. First line in file is the field name for the columns and the rest are the data in the table so I separate them when reading the file in to the table. The `int x=0` is only used for checking whether to fill in the field name or the data rows. It changes to `1` when the field name is filled so the program knows to put the data in the rows instead of the field name.

I split each data with a `,` so I used `.split(",")` and then trim the excess space out with `.trim()`.

For writing out files I used `BufferedWriter` and write out files separate by field names and the data just like what I did when reading files.

* `public void readFile(Table table, String f)`
* `public void writeFile(Table table, String f)`


### Printing
`Printing.java`

I used `System.out.printf()` so I can format them and the table can align neatly. I let the data align on the left hand side separating each data with 10 space (including the data itself) using `"%-10s"`.


### Keys
Most of the changes I made for `keys` part is in the `Table` class. I added a `searchRow(String s)` method to the `Table` class to search through the table's first column to check if a same key has already appeared in the table.

* `public boolean searchRow(String s)` returns `false` if a key has already appeared in the table and `true` if the key has not appear in the table.
* `private int rowCount=0` is also added. Increments when a row is added and decrements when a row is removed.

I changed the `insertRow` method from `void` type to `boolean` type so I can check if a row is successfully added or not.

* `public boolean insertRow(String... s)` now checks if a same key is already in the table or not. It calls `searchRow(String s)` before adding data into the table so if a key is already in a table, it will not add the row.
* `public Record selectKey(String s)` returns the row by selecting only the key.


### Databases

`Database.java`

For the `Database` part I made a method to create a folder and a method to read multiple text files altogether from a same directory and print the tables out.

* `private void createFolder()`
* `private void readMulFiles()`

Now I can read in multiple files in one go. When I was testing, I used the method above to create a new folder calls `New_Folder` and I used two tables to test it using the `writeFile()` method in the `StoreFile` class. Before putting data into the second table I have to clear the first table so I created a new method `clearAll()` in `Record` class to clear the field name. I still used `deleteRow()` to clear the rows so the program can keep track of the `rowCount`.

* `public void clearAll()`


### Final Changes

After finish most of the work I started changing most of the `void` type methods to `boolean` so I can have a more clear way to test things using `assert`. I also changed the `insertRow()` method in `Table` class so now it only adds to the row if the input data has the same number as the column.


### All Done!
