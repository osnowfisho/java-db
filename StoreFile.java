/*
read files into a table and write table out to a text file
*/

import java.util.*;
import java.io.*;

class StoreFile {
	public static void main(String[] args) {
		StoreFile file = new StoreFile();
		file.run();
	}

	public boolean readFile(Table table, String f) {
		BufferedReader reader;
		int x=0;
		try {
			reader = new BufferedReader(new FileReader(f));
			String line = reader.readLine();
			while (line != null) {
				String[] str = line.split(",");
				for (int i=0; i<str.length ; i++) {
					str[i] = str[i].trim();
				}
				if (x == 0) {
					table.setFieldName(str);
					x=1;
				}
				else {
					table.insertRow(str);	
				}
				line = reader.readLine();
			}
			reader.close();
			return true;
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}

	public boolean writeFile(Table table, String f) {
		BufferedWriter writer;
		String s;
		try {
			writer = new BufferedWriter(new FileWriter(f));
			for (int i=0; i<table.getFieldNameSize(); i++) {
				s = table.getFieldName().getField(i);
				writer.write(s + ", ");
			}
			writer.newLine();
			for (int i=0; i<table.getRowNumber(); i++) {
				for (int j=0; j<table.getFieldNameSize(); j++) {
					s = table.selectRow(i).getField(j);
					writer.write(s + ", ");
				}
				writer.newLine();
			}
			writer.close();
			return true;
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}

	private void run() {
		testRead();
		testWrite();
	}

	private void testRead() {
		Table table = new Table();

		assert(readFile(table, "pet.txt"));

		assert(table.getFieldName().getField(0).equals("Id"));
		assert(table.selectRow(0).getField(0).equals("1"));
		assert(table.selectRow(1).getField(1).equals("Wanda"));
		assert(table.selectRow(2).getField(3).equals("ab123"));
	}

	private void testWrite() {
		Table table = new Table();

		assert(table.setFieldName("Username", "Name"));
		assert(table.insertRow("ab123", "Jo"));
		assert(table.insertRow("cd456", "Sam"));
		assert(table.insertRow("ef789", "Amy"));
		assert(table.insertRow("gh012", "Pete"));

		assert(writeFile(table, "name.txt"));
	}
}