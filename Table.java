/*
make changes to the whole row instead of one field
*/

import java.util.*;

class Table {
	private Record fieldName = new Record();
	private ArrayList<Record> row = new ArrayList<Record>();
	private int rowCount=0;

	public static void main (String[] args) {
		Table table = new Table();
		table.run();
	}

	public boolean setFieldName(String... s) {
		try {
			fieldName.setField(s);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public boolean insertRow(String... s) {
		Record r = new Record();
		if (searchRow(s[0])) {
			if (getFieldNameSize() == s.length) {
				row.add(r);
				r.setField(s);
				rowCount++;
				return true;
			}
		}
		return false;
	}

	public Record selectRow(int i) {
		return row.get(i);
	}

	public boolean deleteRow(int i) {
		try {
			row.remove(i);
			rowCount--;
			return true;		
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public boolean updateRow(int i, String... s) {
		try {
			for (int j=0; j<getFieldNameSize(); j++) {
				row.get(i).updateField(j, s[j]);
			}
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	// get number of columns
	public int getFieldNameSize() {
		return fieldName.getFieldSize();
	}

	public boolean alterTable(String s) {
		try {
			fieldName.setField(s);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public Record getFieldName() {
		return fieldName;
	}

	public int getRowNumber() {
		return rowCount;
	}

	public boolean searchRow(String s) {
		for (int i=0; i<rowCount; i++) {
			if (selectRow(i).getField(0).equals(s)) {
				return false;
			}
		}
		return true;
	}

	public Record selectKey(String s) {
		for (int i=0; i<rowCount; i++) {
			if (selectRow(i).getField(0).equals(s)) {
				return selectRow(i);
			}
		}
		return fieldName;
	}

	private void run() {
		testInsertFieldName();
		testInsertRow();
		testDeleteRow();
		testUpdateRow();
		testAlterTable();
		testSelectKey();
	}

	private void testInsertFieldName() {
		assert(setFieldName("Id", "Name", "Kind", "Owner"));

		assert(fieldName.getField(0).equals("Id"));
		assert(!fieldName.getField(1).equals("Id"));
		assert(fieldName.getField(2).equals("Kind"));
	}

	private void testInsertRow() {
		assert(insertRow("1", "Fido", "dog", "ab123"));
		assert(!insertRow("1", "Fido", "dog", "ab123"));
		assert(insertRow("2", "Wanda", "fish", "ef789"));
		assert(!insertRow("2", "Garfield", "cat", "ab123"));
		assert(!insertRow("3", "Garfield", "cat", "cat", "ab123"));
		assert(insertRow("3", "Garfield", "cat", "ab123"));
		
		assert(selectRow(0).getField(0).equals("1"));
		assert(!selectRow(1).getField(0).equals("1"));
		assert(selectRow(1).getField(3).equals("ef789"));
		assert(selectRow(2).getField(1).equals("Garfield"));
		assert(getRowNumber() == 3);
	}

	private void testDeleteRow() {
		assert(row.get(1).getField(0).equals("2"));
		assert(row.get(1).getField(3).equals("ef789"));

		assert(deleteRow(1));

		assert(!selectRow(1).getField(0).equals("2"));
		assert(selectRow(1).getField(0).equals("3"));
		assert(selectRow(1).getField(1).equals("Garfield"));
		assert(!selectRow(1).getField(3).equals("ef789"));

		assert(getRowNumber() != 3);
		assert(getRowNumber() == 2);
	}

	private void testUpdateRow() {
		assert(selectRow(1).getField(0).equals("3"));
		assert(selectRow(1).getField(2).equals("cat"));	

		assert(updateRow(1, "2", "Wanda", "fish", "ef789"));

		assert(selectRow(1).getField(0).equals("2"));
		assert(!selectRow(1).getField(1).equals("Garfield"));
		assert(selectRow(1).getField(2).equals("fish"));	
	}

	private void testAlterTable() {
		assert(fieldName.getField(0).equals("Id"));
		assert(alterTable("People"));
		assert(fieldName.getField(4).equals("People"));
	}

	private void testSelectKey() {
		assert(selectKey("1").getField(2).equals("dog"));
		assert(selectKey("2").getField(1).equals("Wanda"));

		fieldName.clearAll();
		assert(deleteRow(1));
		assert(deleteRow(0));
		assert(!deleteRow(0));

		assert(setFieldName("Username", "Name"));
		assert(insertRow("ab123", "Jo"));
		assert(insertRow("cd456", "Sam"));
		assert(insertRow("ef789", "Amy"));
		assert(insertRow("gh012", "Pete"));

		assert(selectKey("ab123").getField(1).equals("Jo"));
		assert(selectKey("ef789").getField(1).equals("Amy"));
	}
}
